# TP3 : Réseaux Docker

Construire une application en utilisant plusieurs conteneurs fonctionnant en parallèle
- [La communication entre containers](#t1)
- [Comment configurer un réseau à partager entre les conteneurs](#t2)
- [Exercice 1 : créer un réseau dédié pour deux containers](#t3)
- [Comment lancer une application basée sur plusieurs conteneurs ?](#t4)
- [Exemple 1 : Lancez une interface web pour Docker](#t5)
- [Exemple 2 : Exécutez votre propre Wordpress](#t6)
- [Exercice 2 : créer une application multi-conteneur](#t7)

# Contenu du TP

- Durée estimée : 4 heures.
- Travail à rendre : le résultat des sections exercices [1](#t3) et [2](#t7) du TP.

### Préparez-vous à livrer vos changements

**Créez une branche git** nommée *$group/$USER* et travaillez à partir de celle-ci. Avant de partir, **commitez et poussez votre travail sur cette branche**. Il sera utilisé dans le cadre de votre **évaluation**. Mettez votre **groupe de tp et votre login en minuscules.**

Pour créer une branche git et passer dessus, une commande possible est :

```
git checkout -b $group/$USER
```

**AVANT DE COMMENCER A MODIFIER CE PROJET : **

Lisez bien le paragraphe précédent. **Créez et travaillez sur votre branche!**
Si vous avez du mal avec Git, sachez qu'il est plus simple de travailler sur la bonne branche dès le début que de s'énerver en fin de séance à essayer de pousser sur master, qui est protégée en écriture. En particulier aux heures suivantes : 9h52, 11h59, 15h17 et 17h23.



## Vos containers peuvent communiquer entre eux <a name="t1"></a>

Dans les TPs précédents, les containers n'ont pas eu l'occasion de communiquer avec le monde extérieur, ou d'autres containers. Ils n'étaient que l'ombre d'eux-mêmes.

![Container isolé](pictures/castaway.png)

*Ci-dessus, une image prise sur le vif d'un container busybox du tp1.*

Non seulement ils en sont capables, mais cette **communication est la base pour des architectures logicielles distribuées**, avec un container par agent / tâche.

Dans l'exemple suivant, nous allons faire **tourner un serveur web dans un premier conteneur, puis envoyer une requête HTTP depuis un second**.

Pour **lancer un serveur web** (ici en utilisant nginx) et le laisser tourner en arrière-plan:

```
docker run --rm --name mynginx --detach nginx
```

Ensuite, pour **récupérer son adresse IP** actuelle :

```
raph@raph-VirtualBox:~$ docker inspect mynginx | grep IPAddress
            "SecondaryIPAddresses": null,
            "IPAddress": "172.17.0.2",
                    "IPAddress": "172.17.0.2",
```

Depuis un autre conteneur, on peut maintener **faire une requête** à ce serveur :

```
raph@raph-VirtualBox:~$ docker run -it busybox
/ # wget -q -O - 172.17.0.2:80
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
html { color-scheme: light dark; }
body { width: 35em; margin: 0 auto;
font-family: Tahoma, Verdana, Arial, sans-serif; }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
```

Les **deux containers ont échangé des données**! Comment ? Grâce aux **réseaux docker**.


## Réseau Docker <a name="t2"></a>

### Concept

Docker peut isoler le [réseau du conteneur](https://docs.docker.com/network/bridge/) de l'hôte. Lorsqu'un conteneur est créé, Docker crée également un **namespace réseau** distinct. Par défaut, Docker crée un **pont (bridge)** appelé **docker0** vers l'hôte. Il n'y a généralement qu'une seule instance du pont par démon Docker, connectant les namespaces réseaux des conteneurs au réseau de l'hôte. La connexion réelle entre le pont et les réseaux des conteneurs est assurée par une **paire de veth**, qui agit comme une carte d'interface réseau (NIC) virtuelle. Il y en a une par conteneur **actif**. Dans la plupart des conteneurs Linux, l'interface par défaut est **eth0**.

![texte alt](pictures/veth.png)
*Image : dev.to/polarbit*.

```
raph@raph-VirtualBox:~/iut/week_3$ docker run -it busybox:1.34.1
/ # ifconfig
eth0 Link encap:Ethernet HWaddr 02:42:AC:11:00:03
          inet addr:172.17.0.3 Bcast:172.17.255.255 Mask:255.255.0.0
          UP BROADCAST RUNNING MULTICAST MTU:1500 Metric:1
          Paquets RX:15 erreurs:0 abandonnés:0 dépassements:0 trames:0
          Paquets TX:0 erreurs:0 abandonnés:0 dépassements:0 porteuse:0
          collisions:0 txqueuelen:0
          Octets RX:2203 (2.1 KiB) Octets TX:0 (0.0 B)

lo Link encap:Local Loopback
          inet addr:127.0.0.1 Mask:255.0.0.0
          UP LOOPBACK RUNNING MTU:65536 Metric:1
          Paquets RX:0 erreurs:0 abandonnés:0 dépassements:0 trames:0
          Paquets TX:0 erreurs:0 abandonnés:0 dépassements:0 porteuse:0
          collisions:0 txqueuelen:1000
          Octets RX:0 (0.0 B) Octets TX:0 (0.0 B)

/ # exit
raph@raph-VirtualBox:~/iut/week_3$ ip addr
1 : lo : <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2 : enp0s3 : <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:24:83:72 brd ff:ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 83640sec preferred_lft 83640sec
    inet6 fe80::bf5c:61a6:2d95:aa0a/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3 : docker0 : <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default
    link/ether 02:42:45:63:e2:b6 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
       valid_lft forever preferred_lft forever
    inet6 fe80::42:45ff:fe63:e2b6/64 scope link
       valid_lft forever preferred_lft forever
5 : vethedbec2b@if4 : <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master docker0 state UP group default
    link/ether 22:93:7f:19:c7:15 brd ff:ff:ff:ff:ff link-netnsid 0
    inet6 fe80::2093:7fff:fe19:c715/64 scope link
       valid_lft forever preferred_lft forever

```

Vous pouvez **lister les réseaux docker** avec la commande client docker :

```
raph@raph-VirtualBox:~$ docker network ls
RÉSEAU ID NOM PILOTE PORTÉE
4862843da9d5 bridge bridge local
b2c6603cea69 host host local
2a149c2c7847 none null local
```

Les réseaux ci-dessus sont **prédéfinis et ne peuvent pas être supprimés** :
- Par défaut, les containers utilisent le réseau **bridge**. C'est grâce à ce réseau que précédemment le container busybox a pu envoyer une requête HTTP au container nginx.

- Le réseau **host** permet à un container de sauter quelques étapes et de directement **utiliser l'interface réseau de l'hôte**. Dans ce cas il n'y a **pas de NAT**, ce qui est pratique quand beaucoup de ports sont sollicités, mais le container s'expose alors à des **conflits si l'hôte utilise déjà ces ports**. Certaines applications ont besoin d'un niveau d'accès plus élevé au réseau, par exemple un [serveur DHCP](https://hub.docker.com/r/networkboot/dhcpd/).

- Le réseau **none** est utilisé pour créer des **containers sans accès réseau**, il désactive la création d'une interface réseau dans le container.

Les réseaux sont gérés par 2 pilotes :
- **Bridge** (par défaut) : Les ponts sont isolés au sein d'une seule instance du démon docker. Le pilote et le réseau bridge ont le même nom, mais attention! Ce sont des concepts séparés.
- **Overlay** : En utilisant ce pilote, il est possible de relier des conteneurs fonctionnant sur différentes instances du démon docker. Il est utilisé dans des grappes d'hôtes Docker, généralement un [Docker Swarm](https://docs.docker.com/engine/swarm/). La configuration de Docker désactive la création de ce type de réseau en dehors du contexte d'un Docker Swarm.

### Créer et utiliser un réseau Docker personnalisé

Un réseau docker peut être créé avec une seule commande :

```
raph@raph-VirtualBox:~$ docker network create -d bridge my-bridge
608965114eccc5cf93089d7a91a9a9c15856aa4a694af19c9948b3fdb66fa991
raph@raph-VirtualBox:~$ docker network ls
RÉSEAU ID NOM PILOTE PORTÉE
4862843da9d5 bridge bridge local
b2c6603cea69 host host local
608965114ecc my-bridge bridge local
2a149c2c7847 none null local
```

L'option -d ici est facultative pour le réseau bridge, car c'est le pilote par défaut.

Vous pouvez essayer de créer un réseau basé sur le driver **overlay**, mais il est généralement désactivé :

```
raph@raph-VirtualBox:~$ docker network create -d overlay my-overlay
Réponse d'erreur du démon : Ce nœud n'est pas un gestionnaire d'essaim. Utilisez "docker swarm init" ou "docker swarm join" pour connecter ce nœud à l'essaim et réessayez.
```


Docker crée toujours des réseaux sur des plages d'IP libres pour vous, mais vous pouvez **personnaliser ces nouveaux réseaux** à la création :

```
docker network create --subnet=192.168.66.0/24 custom-network
```

Vous pouvez fournir le **réseau à la création du conteneur** :

```
docker run -it --network=custom-network busybox:1.34.1
```

Ou vous pouvez l'**attacher à un conteneur existant** :

```
docker network connect <nom du pont> <nom du conteneur>
```

Vous pouvez même **partager un réseau entre deux conteneurs** :

```
docker run -it --network container:<nom|id> busybox:1.34.1
```

Vous pouvez **forcer une IP de conteneur** (en supposant que vous avez fait correspondre le sous-réseau) :

```
docker run -it --network=custom-network --ip 192.168.66.12 busybox:1.34.1
```

Enfin, vous pouvez **supprimer un réseau docker** avec la commande `docker network rm`.

Si vous voulez en savoir plus sur les types plus **exotiques** de réseaux Docker, la vidéo ci-dessous est une bonne introduction sur le sujet :

[Docker Networking](https://www.youtube.com/watch?v=bKFMS5C4CG0)

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/bKFMS5C4CG0"  frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

Pas d'inquiétudes, les [IPVLAN](https://docs.docker.com/network/ipvlan/) ou [MACVLAN](https://docs.docker.com/network/macvlan/) ne seront pas à l'examen. Retenez juste les **réseaux pré-définis** et les deux types **bridge** et **overlay**.

Une question légitime à ce stade : pourquoi s'embêter à créer un réseau docker pour faire communiquer nos deux conteneurs quand le réseau par défaut suffit dans l'exemple avec nginx? Il y a plusieurs raisons :
- **Sécurité** : un conteneur est isolé des ressources qu'il n'a pas à utiliser. Les réseaux font partie de ces ressources. De cette manière, on limite l'action d'un agent malveillant au maximum.
- **Parallélisme** : un conteneur peut être instantié N fois sur N réseaux différents pour accroître la disponibilité d'un service. C'est très commun pour des serveurs webs par exemple.
- **Maintenance** : si un ensemble de conteneurs crashent ou se comporte de façon suspecte, il vaut mieux qu'ils soient sur le réseau le plus petit possible pour limiter le périmètre d'enquête et de déboggage.

## Exercice : créer un réseau dédié pour deux containers <a name="t3"></a>

Créez un fichier texte appelé **exo1.sh** dans la racine du projet.

Créez un réseau de classe C avec docker, appelé **exo1_net**.

**Relancez les deux containers du début de ce TP (nginx et busybox) en leur assignant le réseau exo1_net. Vérifiez que les deux containers peuvent communiquer sur ce réseau dédié**.

**Dans le fichier exo1.sh, écrivez les 3 lignes** suivantes :
- La ligne utilisée pour créer le réseau exo1_net.
- La ligne utilisée pour lancer un container nginx dans ce réseau.
- La ligne utilisée pour lancer un container busybox dans ce réseau.

Dès la fin de cet exercice, commitez votre travail.


## Comment lancer une application basée sur des conteneurs multiples <a name="t4"></a>

Les applications basées sur des conteneurs mélangent des volumes et des ponts pour envoyer et recevoir des informations en interne. La mise en place d'une telle application nécessite les étapes suivantes :
- **Séparer votre application en services**. Exemple : une simple application web client-serveur peut être composée de trois éléments : frontend, backend, et une base de données.
- Tirez ou construisez les **images**.
- Créez les **volumes et réseaux** de Docker.
- **Lancez les conteneurs avec la bonne configuration** : ports à ouvrir, configuration d'exécution pour chaque conteneur. Un conteneur gère généralement un service dans l'application.
- **Surveiller l'état des conteneurs Docker**. Vous pouvez surveiller tous les conteneurs en temps réel avec la commande `docker stats --all` :

```
CONTENEUR ID NOM CPU % MEM USAGE / LIMITE MEM % NET I/O BLOCK I/O PIDS
7dff7f1876ad test 0.00% 556KiB / 7.772GiB 0.01% 2.94kB / 0B 0B / 0B 1
2143d19880f3 funny_shockley 0,00% 0B / 0B 0,00% 0B / 0B 0B / 0B 0
c1a0aee24196 cool_sutherland 0,00% 0B / 0B 0,00% 0B / 0B 0B / 0B 0
18e09dd4edbe quirky_kirch 0,00% 0B / 0B 0,00% 0B / 0B 0B / 0B 0
24947bf06d1c keen_ellis 0,00% 0B / 0B 0,00% 0B / 0B 0B / 0B 0
d1bd27310675 serene_wozniak 0,00% 0B / 0B 0,00% 0B / 0B 0B / 0B 0
b971dd8bc5c5 hungry_noyce 0,00% 0B / 0B 0,00% 0B / 0B 0B / 0B 0
54ae65de4746 charming_mendel 0,00% 0B / 0B 0,00% 0B / 0B 0B / 0B 0

```

Vous pouvez **définir la politique sur le changement de statut**. Ceci est généralement fait lors de la création du conteneur.

![texte alt](pictures/restart_policy.png)
*Image : docs.docker.com*.

Par exemple, cette ligne crée un registre docker privé et expose son port 5000 au port 1234 de l'interface hôte.
Sa **politique de redémarrage** est de se relancer automatiquement, si pour une raison quelconque le conteneur s'arrêtait :

`docker run -d -p 5000:1234 --restart=always --name my-registry registry:2`

## Exemple 1 : Lancez une interface web pour Docker <a name="t5"></a>

Le client Docker a été jugé trop austère par certaines personnes de mauvais goût, c'est pourquoi il existe aujourd'hui de nombreuses **interfaces web**. L'une d'entre elles est [portainer](https://www.portainer.io/), et elle disponible sur le docker Hub :

```
docker run --detach --name portainer \
           -p 9000:9000 \
		   -v portainer_data:/data \
           -v /var/run/docker.sock:/var/run/docker.sock \
		   portainer/portainer-ce:alpine
```

Une fois lancée, cette interface est disponible au locahost port 9000. Il faudra simplement créer un utilisateur admin la première fois (ou si vous supprimez le volume docker portainer_data).

![Page d'acceuil de portainer](pictures/portainer.png)

Cette application n'est pas multi-container, mais si vous avez un peu de mal à repérer l'état actuel de votre démon docker, elle pourra vous être utile.

## Exemple 2 : Exécutez votre propre Wordpress <a name="t6"></a>

Enfin une véritable application multi-container!

Les lignes suivantes vous permettront d'exécuter une application web [Wordpress](https://wordpress.com/) en quelques commandes seulement, en utilisant docker :

Tout d'abord, créez le volume utilisé pour le stockage des données persistantes :

```
docker volume create db_data
```

Ensuite, exécutez le **conteneur avec le service de base de données** :

```
docker run --name=db -d -v db_data:/var/lib/mysql --restart=always -e MYSQL_ROOT_PASSWORD=somewordpress -e MYSQL_DATABASE=wordpress -e MYSQL_USER=wordpress -e MYSQL_PASSWORD=wordpress mysql:5.7
```

Vous pouvez maintenant lancer le **conteneur avec le front wordpress** :

```
docker run -d --name=wp -p 8000:80 --restart=always -e WORDPRESS_DB_HOST=db:3306 -e WORDPRESS_DB_USER=wordpress -e WORDPRESS_DB_PASSWORD=wordpress wordpress:latest
```

La dernière étape consiste à **connecter les deux containers au même réseau** :

```
docker network create my_wordpress_default
docker network connect my_wordpress_default db
docker network connect my_wordpress_default wp
```

L'application est maintenant disponible dans votre navigateur Web, à l'adresse [localhost:8000](localhost:8000).

![texte alt](pictures/home_wordpress.png)

![alt text](pictures/home_wordpress_2.png)

Avec Docker, il devient **extrêmement simple de (re)déployer des services**! Vous pouvez ainsi installer en quelques minutes, sur votre réseau domestique, un service de [streaming](https://hub.docker.com/r/tiangolo/nginx-rtmp/), un [cloud](https://hub.docker.com/r/owncloud/server/) ou un serveur [Minecraft](https://hub.docker.com/r/itzg/minecraft-server) ou [CSGO](https://hub.docker.com/r/cm2network/csgo). Vous pouvez ensuite exposer le service sur Internet en configurant les ports de votre routeur (si vous ne savez pas ce que vous faites, ne le faites pas!).

Avant Docker, beaucoup de temps était perdu à essayer de restaurer une application hors-service, que ce soit en triturant sa configuration ou en se débattant avec les dépendances systèmes.

Aujourd'hui, si un **container d'application cesse de fonctionner, n'ayez aucun remord à le détruire pour en relancer un autre**. C'est rapide et facile, et c'est la même philosophie que lorsque vous revenez à un commit fonctionnel dans un dépot Git.

## Exercice : créer une application multi-conteneur <a name="t7"></a>
Le but de cette exercice est de vous entraîner à relier des conteneurs entre eux pour réaliser une application multi-conteneur. Dans le dossier *exo2/*, vous trouverez :

- Un dossier *hub/* contenant les ressources pour compiler l'image **hub**.
- Un dossier *client/* contenant les ressources pour compiler l'image **client**.

Vous devez créer dans *exo2/* un fichier **exo2.sh** qui devra :
- Créer 6 réseaux Docker différents, avec une **IPv4 de classe B** chacun.
- Créer un conteneur basé sur l'image **exo2_hub**, relié à tous les réseaux créés plus haut. Ce conteneur aura le dossier *exo2/hub/* partagé avec **nginx** monté dans */data*.
- Créer un conteneur basé sur **nginx:1.23**, avec *exo2/hub/* monté dans */usr/share/nginx/html*.
- Créer 6 conteneurs basés sur l'image **exo2_client**, chacun **relié à un seul réseau** précédemment créé.
- Paramétrer les conteneurs au lancement pour définir les ports utilisés.

![Diagramme de l'application multi-conteneur attendu pour l'exercice 2](pictures/tp3_exo2_diag.png)

Le conteneur de **hub** met à jour la page web hébergé par **nginx** périodiquement. Ce conteneur attend des messages périodiques de chaque client, et le contenu de la page html affichée n'est complet que si tous les clients ont envoyé un message récemment (5s).

Les conteneurs **client** s'arrêtent une fois qu'ils ont envoyé leur paquet, il faut donc régler leur politique de redémarrage pour qu'ils se relancent automatiquement, comme montré dans la section [Comment lancer une application basée sur des conteneurs multiples](#t4).

Si la page web est complète, vous verrez s'afficher le message suivant :
`Page complète : Exo 2 OK!`

Hormis exo2.sh, vous n'avez rien à modifier dans les fichiers existants du dossier *exo2/*.

## Auteurs

Raphaël Bouterige
