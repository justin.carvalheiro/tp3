import os
import socket
import time

sleep_s = float(os.getenv("SLEEP_S", 11.0))
dest_ip = os.getenv("DEST_IP", "127.0.0.1")
dest_port = int(os.getenv("DEST_PORT", 5000))
packet_payload = int(1).to_bytes(1, byteorder='little')

# Create socket for server
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
print(f"[{time.time():.3f}] Sending packet to {dest_ip}:{dest_port} and sleep {sleep_s:.2f}s")
s.sendto(packet_payload, (dest_ip, dest_port))
s.close()

time.sleep(sleep_s)
print(f"[{time.time():.3f}] Done")

