from datetime import datetime, timezone
import os
import random
import re
import select
import signal
import socket
import subprocess
import sys
import time 

keep_going = True
required_connections = 6
connection_timeout_s = 15.
refresh_timeout_s = 3.


def create_socket_on_port(port_number: int):
    # Create a non-blocking UDP socket
    new_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    new_socket.setblocking(0)
    new_socket.bind(("0.0.0.0", port_number))
    return new_socket


def display_interfaces(ports_list):
    ip_addr_cmd_res = subprocess.getoutput("ip addr | grep inet")
    ip_interfaces = []
    for line in ip_addr_cmd_res.split("\n"):
        ip_interfaces.append(line.split("inet ")[1].split("/")[0])
    print(f"[{time.time():.3f}] En écoute sur les ports {ports_list} et interfaces : {ip_interfaces}")


def refresh_web_page(clients_history):
    page_path = "/data/index.html"
    regex_date = r"(<h1>)(.*)(</h1>)"
    regex_status = r"(<div>)(.*)(</div>)"
    regex_img = r"(images/)(.*)(.jpg)"

    if os.path.isfile(page_path) is False:
        raise FileNotFoundError(f"Impossible de trouver la page web {page_path}")

    page_data = ""
    with open(page_path, "r", encoding='utf-8') as page:
        page_data = page.read()

    utc_dt = datetime.now(timezone.utc)
    subst = f"\\1Date : {utc_dt.astimezone().isoformat()}\\3"
    new_page_data = re.sub(regex_date, subst, page_data, 0, re.MULTILINE)

    connection_data = "<ul>"
    connected_client_cnt = 0
    connected_interfaces = set()
    for port in clients_history:
        last_connection_time = clients_history[port][0]
        last_connection_addr = clients_history[port][1]
        if last_connection_time < 0:
            connection_data += f"<li>Port {port} : Aucun</li>"
        else:
            dt = time.time() - last_connection_time
            if dt > connection_timeout_s:
                connection_data += f"<li>Port {port} : Il y a {dt:.3f}s from {last_connection_addr} (timeout)</li>"
            else:
                connected_interfaces.add('.'.join(last_connection_addr.split(".")[:2]))
                connected_client_cnt += 1
                connection_data += f"<li>Port {port} : Il y a {dt:.3f}s from {last_connection_addr}</li>"
    connection_data += "</ul>"
    

    connection_data += f"<br>{connected_client_cnt} connections actives de {len(connected_interfaces)} interface(s) différente(s)<br>"
    if connected_client_cnt == len(connected_interfaces) == required_connections:
        connection_data += "<br><b>Page complète : Exo 2 OK!</b><br>"
    else:
        connection_data += "<br>Page incomplète : Exo 2 KO<br>"
    
    subst = f"\\1{connection_data}\\3"
    new_page_data = re.sub(regex_status, subst, new_page_data, 0, re.MULTILINE)
    
    subst = f"\\1l{min(connected_client_cnt, len(connected_interfaces))}\\3"
    new_page_data = re.sub(regex_img, subst, new_page_data, 0, re.MULTILINE)
    
    with open(page_path, "w", encoding='utf-8') as page:
        page.write(new_page_data)


def signal_handler(sig, frame):
    global keep_going
    if sig == signal.SIGINT:
        keep_going = False
    if sig == signal.SIGTERM:
        sys.exit(0)


def main():
    global keep_going

    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)

    n_port = int(os.getenv("LISTEN_TO_N_PORTS", 5))
    first_port = int(os.getenv("FIRST_PORT", 0))
    if first_port <= 1024:
        first_port = random.randint(5000, 5300)
    ports_list = list(range(first_port, first_port + n_port))
    sockets = [create_socket_on_port(p) for p in ports_list]

    print(f"Configuration du hub : écoute de {n_port}  port(s) à partir de {first_port}")
    clients_history = {p:[-1, "unknown"] for p in ports_list}


    while keep_going:
        display_interfaces(ports_list)

        readable, _, _ = select.select(sockets, [], [], refresh_timeout_s)

        for client in readable:
            try:
                data, address = client.recvfrom(1)
                clients_history[client.getsockname()[1]] = [time.time(), address[0]]
                state = f"[{time.time():.3f}] Received '{data.hex()}' from {address}"
                print(state)
            except (BlockingIOError, TimeoutError) :
                print(f"Nothing from {client}")

        refresh_web_page(clients_history)

        
    print("End of server refresh script")


main()
